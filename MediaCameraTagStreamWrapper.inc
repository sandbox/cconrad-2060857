<?php

/**
 * @file media_cameratag/MediaCameraTagStreamWrapper.inc
 *
 *  Create a Camera Tag Stream Wrapper class for the Media/Resource module.
 */

/**
 * Create an instance like this:
 *  $cameratag = new MediaCameraTagStreamWrapper('cameratag://uuid/[video-uuid]');
 *
 * Internal URI parameters:
 *  uuid string Unique ID of the Camera Tag video (required)
 *  recording_format string One of qvga, 360p, vga, 720p (optional)
 *  file_format string One of mp4, webm, thumb, small_thumb (optional)
 */
class MediaCameraTagStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://cameratag.com/videos/';
  protected $meta_data = NULL;

  /**
   * Utility function to return defaults for unset parameters.
   */
  protected function _get_default_parameters() {
    return array(
      'uuid'             => NULL,
      'file_format'      => 'mp4',
      'recording_format' => NULL
    );
  }

  /**
   * Utility function to return parameters.
   */
  public function get_parameters() {
    return array_merge($this->_get_default_parameters(), $this->parameters);
  }

  /**
   * Called from template_preprocess_file_entity before theming the video,
   * so most likely unnecessary. Because this might result in fetching the
   * metadata we return an empty string from this and retrieve the real
   * external URL using @see interpolateUrl. Yes, this is a dirty hack, but
   * as long as there is no way to know the recording format of a video UUID
   * the performance penalty feels to high.
   *
   */
  public function getExternalUrl() {
    return '';
  }

  /**
   * Returns a url similar to "http://cameratag.com/videos/0c22afb0-e30b-0130-2d06-22000aec8aba/qvga/mp4".
   */
  public function interpolateUrl($file_format = NULL) {
    $parts = $this->get_parameters();

    if (!$parts['uuid']) {
      throw new Exception('Can\'t create URL without video UUID');
    }

    if (!$parts['recording_format']) {
      $this->parameters['recording_format'] = $this->getHighestRecordingFormat();
      $parts                                = $this->get_parameters();
    }

    // assemble and return interpolated URL
    return $this->base_url . $parts['uuid'] . '/' . $parts['recording_format'] . '/' . ($file_format ? $file_format : $parts['file_format']);
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/cameratag';
  }

  function getTarget($f) {
    return FALSE;
  }

  function getThumbnailPath() {
    return $this->interpolateUrl('thumb');
  }

  function getSmallThumbnailPath() {
    return $this->interpolateUrl('small_thumb');
  }

  /**
   * Utility function to determine the highest recording format (resolution) of a Camera Tag video by querying its metadata.
   *
   * @return string Highest recording format (resolution) available for this video.
   * @throws Exception 'Could not determine highest recording format for video ...'
   */
  protected function getHighestRecordingFormat() {
    $metaData = $this->getMetadata();
    $desired_recording_formats = array(
      '720p',
      'vga',
      '360p',
      'qvga'
    );
    foreach ($desired_recording_formats as $possible_recording_format) {
      foreach ($metaData->formats as $available_recording_format) {
        if ($available_recording_format->name == $possible_recording_format) {
          return $possible_recording_format;
        }
      }
    }

    throw new Exception('Could not determine highest recording format for video ' . $this->getUri() . '. Please specify one explicitly in the constructor.');
  }

  /**
   * Utility function to retrieve metadata about a video from Camera Tag.
   *
   * Warning: expensive operation.
   *
   * @throws Exception 'API key not set. See admin/config/media/cameratag.'
   * @throws Exception 'Could not fetch metadata for video ...'
   * @return stdClass Metadata about the video
   */
  protected function getMetadata() {
    if ($this->meta_data) {
      return $this->meta_data;
    } else {
      $media_cameratag_apikey = variable_get('media_cameratag_apikey');
      if (!$media_cameratag_apikey) {
        throw new Exception('API key not set. See admin/config/media/cameratag.');
      }

      $parts = $this->get_parameters();

      try {
        $video_info_json = file_get_contents('http://cameratag.com/videos/' . check_plain($parts['uuid']) . '.json?api_key=' . $media_cameratag_apikey);
      } catch (Exception $e) {
        throw new Exception('Could not fetch metadata for video ' . $this->getUri(), $previous = $e);
      }

      $this->meta_data = json_decode($video_info_json);

      return $this->meta_data;
    }
  }
}
