<?php

/**
 * @file media_cameratag//theme/media-cameratag-video.tpl.php
 *
 * Template file for theme('media_cameratag_video').
 *
 * Variables available:
 *  $uri - The media uri for the Camera Tag video (e.g., cameratag://uuid/6611a310-72c4-0130-04c5-123139045d73).
 *  $video_uuid - The unique identifier of the Camera Tag video (e.g., 6611a310-72c4-0130-04c5-123139045d73).
 *  $options - An array containing the Media: Camera Tag formatter options.
 *  $width - The width value set in Media: Camera Tag file display options.
 *  $height - The height value set in Media: Camera Tag file display options.
 *  $autoplay - Prerendered attribute to enable autoplay, if enabled in the Media: Camera Tag file display options.
 *  $loop - Prerendered attribute to enable looping, if enabled in the Media: Camera Tag file display options.
 *  $recording_format - Prerendered attribute to play at a lower resolution format, if selected in the Media: Camera Tag file display options.
 *  @TODO Check if this really exists:
 *    $id - The file entity ID (fid).
 */

?>
<div class="<?php print $classes; ?> media-cameratag-video-<?php print $id; ?>">
  <video class="media-cameratag-player" <?php print $autoplay; ?><?php print $loop; ?><?php print $recording_format; ?>width="<?php print $width; ?>" height="<?php print $height; ?>" data-uuid="<?php print $video_uuid; ?>" />
</div>
