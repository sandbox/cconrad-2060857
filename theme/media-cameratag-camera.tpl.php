<?php

/**
 * @file media_cameratag/theme/media-cameratag-camera.tpl.php
 *
 * Template file for theme('media_cameratag_camera').
 *
 * Variables available:
 *  $classes Rendered string of CSS classes
 *  $id Generated by template_preprocess, the count of how many times this theme hook has been called
 *  $name Name of the element, unique in this form
 *  $camera_uuid Camera Tag UUID of the camera to record to
 *
 * @TODO add attributes for style (width/height), name and data-videobitrate
 * @TODO maybe render the different CSS screens for UI customization too
 */

?>
<div class="<?php print $classes; ?>">
  <camera name="<?php print $name; ?>" id="<?php print $name . '-' . $id; ?>" data-uuid="<?php print $camera_uuid; ?>" />
</div>
