<?php

/**
 * @file media_cameratag/MediaCameraTagBrowser.inc
 *
 * Definition of MediaCameraTagBrowser.
 */

/**
 * Media file selector plugin for recording a video.
 */
class MediaCameraTagRecorder extends MediaBrowserPlugin {
  /**
   * Implements MediaBrowserPluginInterface::access().
   */
  public function access($account = NULL) {
    // @TODO: media_access() is a wrapper for file_entity_access(). Switch to the
    // new function when Media 1.x is deprecated.
    return media_access('create', $account);
  }

  /**
   * Implements MediaBrowserPlugin::view().
   */
  public function view() {
    // Check if the user is able to add remote media.
    if (user_access('record a media_cameratag video')) {
      return array(
        'form' => drupal_get_form('media_cameratag_add', $this->params['types'], $this->params['multiselect'])
      );
    }
  }
}
